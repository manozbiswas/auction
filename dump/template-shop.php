<?php
/**
 * Template name: shop
 */
get_header(); ?>
<section id="main_content">
    <div class="container-fluid">
        <div class="row">
            <div class="main_tag_line">
                &nbsp;
            </div>
        </div>
    </div>
    <div class="container content-wrap">
        <div class="row">
            <div class="col-md-3">
                <div class="sidebar">
                    <?php get_template_part('sidebar');?>
                </div>
            </div>
            <div class="col-md-9">
                <div class="view-controls-list" id="viewcontrols">
                    <a class="gridview active"><i class="fa fa-th fa-2x"></i></a>
                    <a class="listview "><i class="fa fa-list fa-2x"></i></a>
                </div>
                <div class="products contents">
                    <?php// echo do_shortcode('[registrationForm]'); ?>
                    <div id="listgrid">
                        <ul class="list">
                            <li class="product">
                                <div class="list-image">
                                    <img
                                        src="<?php echo get_template_directory_uri() ?>/assets/img/products/barbie_8.jpg"
                                        height="200px" width="250px"/>
                                </div>
                                <div class="list-style">
                                    <div class="list-left">
                                        <span class="title"><a href="#">Lorem ipsum dolor sit</a></span>

                                        <p class="description">Product description goes here. Aliquam tincidunt diam
                                            varius
                                            ultricies auctor.
                                            Vivamus faucibus risus tempus, adipiscing justo
                                        </p>

                                        <div class="icon-group-btn">
                                            <p class="time">00:34:21</p>
                                        </div>
                                    </div>
                                    <div class="list-right">
                                        <span class="price">€ 8.99</span>
                                        <span class="detail"><a class="btn btn-default button_red">Bied mee</a></span>
                                    </div>
                                </div>
                            </li>
                            <li class="product">
                                <div class="list-image">
                                    <img
                                        src="<?php echo get_template_directory_uri() ?>/assets/img/products/barbie_7.jpg"
                                        height="200px" width="250px"/>
                                </div>
                                <div class="list-style">
                                    <div class="list-left">
                                        <span class="title"><a href="#">Lorem ipsum dolor sit</a></span>

                                        <p class="description">Product description goes here. Aliquam tincidunt diam
                                            varius
                                            ultricies auctor.
                                            Vivamus faucibus risus tempus, adipiscing justo</p>

                                        <div class="icon-group-btn">
                                            <p class="time">00:34:21</p>
                                        </div>
                                    </div>
                                    <div class="list-right">
                                        <span class="price">€ 8.99</span>
                                        <span class="detail"><a class="btn btn-default button_red">Bied mee</a></span>
                                    </div>
                                </div>
                            </li>
                            <li class="product">
                                <div class="list-image">
                                    <img
                                        src="<?php echo get_template_directory_uri() ?>/assets/img/products/barbie_6.jpg"
                                        height="200px" width="250px"/>
                                </div>
                                <div class="list-style">
                                    <div class="list-left">
                                        <span class="title"><a href="#">Lorem ipsum dolor sit</a></span>

                                        <p class="description">Product description goes here. Aliquam tincidunt diam
                                            varius
                                            ultricies auctor.
                                            Vivamus faucibus risus tempus, adipiscing justo</p>

                                        <div class="icon-group-btn">
                                            <p class="time">00:34:21</p>
                                        </div>
                                    </div>
                                    <div class="list-right">
                                        <span class="price">€ 8.99</span>
                                        <span class="detail"><a class="btn btn-default button_red">Bied mee</a></span>
                                    </div>
                                </div>
                            </li>
                            <li class="product">
                                <div class="list-image">
                                    <img
                                        src="<?php echo get_template_directory_uri() ?>/assets/img/products/barbie_5.jpg"
                                        height="200px" width="250px"/>
                                </div>
                                <div class="list-style">
                                    <div class="list-left">
                                        <span class="title"><a href="#">Lorem ipsum dolor sit</a></span>

                                        <p class="description">Product description goes here. Aliquam tincidunt diam
                                            varius
                                            ultricies auctor.
                                            Vivamus faucibus risus tempus, adipiscing justo</p>

                                        <div class="icon-group-btn">
                                            <p class="time">00:34:21</p>
                                        </div>
                                    </div>
                                    <div class="list-right">
                                        <span class="price">€ 8.99</span>
                                        <span class="detail"><a class="btn btn-default button_red">Bied mee</a></span>
                                    </div>
                                </div>
                            </li>
                            <li class="product">
                                <div class="list-image">
                                    <img
                                        src="<?php echo get_template_directory_uri() ?>/assets/img/products/barbie_4.jpg"
                                        height="200px" width="250px"/>
                                </div>
                                <div class="list-style">
                                    <div class="list-left">
                                        <span class="title"><a href="#">Lorem ipsum dolor sit</a></span>

                                        <p class="description">Product description goes here. Aliquam tincidunt diam
                                            varius
                                            ultricies auctor.
                                            Vivamus faucibus risus tempus, adipiscing justo</p>

                                        <div class="icon-group-btn">
                                            <p class="time">00:34:21</p>
                                        </div>
                                    </div>
                                    <div class="list-right">
                                        <span class="price">€ 8.99</span>
                                        <span class="detail"><a class="btn btn-default button_red">Bied mee</a></span>
                                    </div>
                                </div>
                            </li>
                            <li class="product">
                                <div class="list-image">
                                    <img
                                        src="<?php echo get_template_directory_uri() ?>/assets/img/products/barbie_3.jpg"
                                        height="200px" width="250px"/>
                                </div>
                                <div class="list-style">
                                    <div class="list-left">
                                        <span class="title"><a href="#">Lorem ipsum dolor sit</a></span>

                                        <p class="description">Product description goes here. Aliquam tincidunt diam
                                            varius
                                            ultricies auctor.
                                            Vivamus faucibus risus tempus, adipiscing justo</p>

                                        <div class="icon-group-btn">
                                            <p class="time">00:34:21</p>
                                        </div>
                                    </div>
                                    <div class="list-right">
                                        <span class="price">€ 8.99</span>
                                        <span class="detail"><a class="btn btn-default button_red">Bied mee</a></span>
                                    </div>
                                </div>
                            </li>
                            <li class="product">
                                <div class="list-image">
                                    <img
                                        src="<?php echo get_template_directory_uri() ?>/assets/img/products/barbie_2.jpg"
                                        height="200px" width="250px"/>
                                </div>
                                <div class="list-style">
                                    <div class="list-left">
                                        <span class="title"><a href="#">Lorem ipsum dolor sit</a></span>

                                        <p class="description">Product description goes here. Aliquam tincidunt diam
                                            varius
                                            ultricies auctor.
                                            Vivamus faucibus risus tempus, adipiscing justo</p>

                                        <div class="icon-group-btn">
                                            <p class="time">00:34:21</p>
                                        </div>
                                    </div>
                                    <div class="list-right">
                                        <span class="price">€ 8.99</span>
                                        <span class="detail"><a class="btn btn-default button_red">Bied mee</a></span>
                                    </div>
                                </div>
                            </li>
                            <li class="product">
                                <div class="list-image">
                                    <img
                                        src="<?php echo get_template_directory_uri() ?>/assets/img/products/barbie_1.jpg"
                                        height="200px" width="250px"/>
                                </div>
                                <div class="list-style">
                                    <div class="list-left">
                                        <span class="title"><a href="#">Lorem ipsum dolor sit</a></span>

                                        <p class="description">Product description goes here. Aliquam tincidunt diam
                                            varius
                                            ultricies auctor.
                                            Vivamus faucibus risus tempus, adipiscing justo</p>

                                        <div class="icon-group-btn">
                                            <p class="time">00:34:21</p>
                                        </div>
                                    </div>
                                    <div class="list-right">
                                        <span class="price">€ 8.99</span>
                                        <span class="detail"><a class="btn btn-default button_red">Bied mee</a></span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>
