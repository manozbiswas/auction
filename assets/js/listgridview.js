$(document).ready(function () {   

var list =$('#listgrid ul');
	//list.fadeIn(1000, function () {
		$('#listgrid ul').removeClass('list').addClass('grid');
		$('#viewcontrols').removeClass('view-controls-list').addClass('view-controls-grid');
		$('#viewcontrols .gridview').addClass('active');
		$('#viewcontrols .listview').removeClass('active');
		//list.fadeIn(1000);
	//});
	$('#viewcontrols a').on('click',function(e) {
		if ($(this).hasClass('gridview')) {
			list.fadeOut(1000, function () {
				$('#listgrid ul').removeClass('list').addClass('grid');
				$('#viewcontrols').removeClass('view-controls-list').addClass('view-controls-grid');
				$('#viewcontrols .gridview').addClass('active');
				$('#viewcontrols .listview').removeClass('active');
				list.fadeIn(1000);
			});						
		}
		else if($(this).hasClass('listview')) {
			list.fadeOut(1000, function () {
				$('#listgrid ul').removeClass('grid').addClass('list');
				$('#viewcontrols').removeClass('view-controls-grid').addClass('view-controls-list');
				$('#viewcontrols .gridview').removeClass('active');
				$('#viewcontrols .listview').addClass('active');
				list.fadeIn(1000);
			});									
		}
		e.preventDefault();
	});
});
