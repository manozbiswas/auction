<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?php wp_title("",true); ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <!-- Place favicon.ico in the root directory -->
    <!-- Latest compiled and minified CSS -->
    <!--    <link rel="stylesheet" href="-->
    <?php //echo get_template_directory_uri() ?><!--/assets/css/bootstrap.min.css">-->
    <!--    <link rel="stylesheet" href="-->
    <?php //echo get_template_directory_uri() ?><!--/assets/css/font-awesome.min.css">-->
    <link
        href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic'
        rel='stylesheet' type='text/css'>
    <link
        href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic'
        rel='stylesheet' type='text/css'>
    <!--    <link rel="stylesheet" href="-->
    <?php //echo get_template_directory_uri() ?><!--/assets/css/normalize.css">-->
    <!--    <link rel="stylesheet" href="-->
    <?php //echo get_template_directory_uri() ?><!--/assets/css/listgridview.css">-->
    <!--    <link rel="stylesheet" href="--><?php //bloginfo("stylesheet_url");?><!--">-->
    <!--    <script src="-->
    <?php //echo get_template_directory_uri() ?><!--/assets/js/vendor/modernizr-2.8.3.min.js"></script>-->
    <?php wp_head(); ?>
</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->
<!-- Site design goes here-->
<section id="header">
    <div class="container-fluid ">
        <div class="row">
            <div class="header_top">
                <div class="menu_bar">
                    <nav class="navbar navbar-default navbar-fixed-top">
                        <div class="container">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                        data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand" href="<?php bloginfo('home'); ?>"> <img
                                        src="<?php echo get_template_directory_uri() ?>/assets/img/logo.png"
                                        alt="logo"/>
                                </a>
                            </div>
                            <div id="navbar" class="navbar-collapse collapse">
                                <ul class="nav navbar-nav navbar-right">
                                    <li><a href="registration.php">Registreren</a></li>
                                    <li><a href="login.html">Inloggen</a></li>
                                    <li><a href="single-product.php">Klantenservice</a></li>
                                </ul>
                            </div>
                            <!--/.nav-collapse -->
                        </div>
                    </nav>
                </div>
            </div>
        </div>
<!--        <div class="row">-->
<!--            <div class="header_bottom ">-->
<!--                <div class="container">-->
<!--                    <p>Word ook winnaar! Bepaal zelf je prijs en bied gratis mee.</p>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
    </div>
</section>
<section id="banner">
    <div class="container-fluid">
        <div class="row">
            <div class="slider">
                <div class="slider_image">
                    <img class="img-responsive" width="100%" height="100%"
                         src="<?php echo get_template_directory_uri() ?>/assets/img/banner.jpg" alt="banner"/>
                </div>
            </div>
        </div>
    </div>
</section>