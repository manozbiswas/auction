<!doctype html>
<html class="no-js" lang="" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>TicketIslam</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <!-- Place favicon.ico in the root directory -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic'
          rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic'
          rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="assets/css/normalize.css">
    <link rel="stylesheet" href="assets/css/listgridview.css">
    <link rel="stylesheet" href="style.css">
    <script src="assets/js/vendor/modernizr-2.8.3.min.js"></script>
</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->
<!-- Site design goes here-->
<section id="header">
    <div class="container-fluid ">
        <div class="row">
            <div class="header_top">
                <div class="menu_bar">
                    <nav class="navbar navbar-default navbar-fixed-top">
                        <div class="container">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                        data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand" href="index.html"> <img src="assets/img/logo.png" alt="logo"/>
                                </a>
                            </div>
                            <div id="navbar" class="navbar-collapse collapse">
                                <ul class="nav navbar-nav navbar-right">
                                    <li><a href="registration.php">Registreren</a></li>
                                    <li><a href="login.html">Inloggen</a></li>
                                    <li><a href="single-product.php">Klantenservice</a></li>
                                </ul>
                            </div>
                            <!--/.nav-collapse -->
                        </div>
                    </nav>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="header_bottom ">
                <div class="container">
                    <p>Word ook winnaar! Bepaal zelf je prijs en bied gratis mee.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="banner">
    <div class="container-fluid">
        <div class="row">
            <div class="slider">
                <div class="slider_image">
                    <img class="img-responsive" width="100%" height="100%" src="assets/img/banner.jpg" alt="banner"/>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="main_content">
    <div class="container-fluid">
        <div class="row">
            <div class="main_tag_line">
                &nbsp;
            </div>
        </div>
    </div>
    <div class="container content-wrap">
        <div class="row">
            <div class="col-md-4">
                <div class="sidebar">
                    <div class="search_bar">
                        <form class="search" method="post" action="">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="zoek veld "/>
                                <span class="input-group-btn">
                                    <button class="btn btn-default button_red" type="button">
                                        <i class="glyphicon glyphicon-search"></i>
                                    </button>
                                </span>
                            </div>
                            <!-- /input-group -->
                        </form>
                    </div>
                    <div class="widget registration-form">
                        <form>
                            <div class="form-group">
                                <h4>Accountgegevens</h4>
                                <input type="email" class="form-control" id="exampleInputEmail1"
                                       placeholder="E-mailadres">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" id=""
                                       placeholder="Wachtwoord">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id=""
                                       placeholder="Bevestig wachtwoord">
                            </div>
                            <h4>Persoonlijke gegevens</h4>

                            <div class="form-group">
                                <input type="text" class="form-control" id=""
                                       placeholder="Voornaam">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id=""
                                       placeholder="Achternaam">
                            </div>
                            <select class="form-control select-control" placeholder="Dag">
                                <option value="00">Dag</option>
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                                <option value="16">16</option>
                                <option value="17">17</option>
                                <option value="18">18</option>
                                <option value="19">19</option>
                                <option value="20">20</option>
                                <option value="21">21</option>
                                <option value="22">22</option>
                                <option value="23">23</option>
                                <option value="24">24</option>
                                <option value="25">25</option>
                                <option value="26">26</option>
                                <option value="27">27</option>
                                <option value="28">28</option>
                                <option value="29">29</option>
                                <option value="30">30</option>
                                <option value="31">31</option>
                            </select>
                            <select class="form-control select-control" placeholder="Maand">
                                <option value="00">Maand</option>
                                <option value="01">Jan</option>
                                <option value="02">Feb</option>
                                <option value="03">Mrt</option>
                                <option value="04">Apr</option>
                                <option value="05">Mei</option>
                                <option value="06">Jun</option>
                                <option value="07">Jul</option>
                                <option value="08">Aug</option>
                                <option value="09">Sep</option>
                                <option value="10">Okt</option>
                                <option value="11">Nov</option>
                                <option value="12">Dec</option>
                            </select>
                            <select class="form-control select-control" placeholder="Jaar">
                                <option selected>Jaar</option>
                                <?php
                                $year = '1997';
                                for($i=date("Y"); $i >= date("Y")- 95; $i--)
                                    if($year == $i)
                                        echo "<option value='$i' selected>$i</option>";
                                    else
                                        echo "<option value='$i'>$i</option>";
                                ?>
                            </select>

                                <h4>Contactgegevens</h4>

                            <div class="form-group">
                                <input type="text" class="form-control" id=""
                                       placeholder="Nederland">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id=""
                                       placeholder="Postcode">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id=""
                                       placeholder="Huisnummer">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id=""
                                       placeholder="Telefoonummer">
                            </div>
                            <h4>Rekeningnummer (IBAN)</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" id=""
                                       placeholder="IBAN">
                            </div>
                            <p>Win je een veiling, dan kun je deze binnen 5
                                dagen zelf betalen. Na 5 dagen gebruiken wij
                                jouw rekeningnummer om het bedrag
                                automatisch te incasseren.</p>
                            <div class="form-group">
                                <button type="submit" class="button_red btn btn-default">Registreren</button>
                            </div>
                            <p>Door op registreren te klikken ga je
                                akkoord met de <a href="">algemene voorwaarden</a></p>

                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="products registration-content">
                    <div class="col-md-3"></div>
                    <div class="col-md-8">
                        <p>Hulp nodig?</p>

                        <p>Onze klantenservice staat voor je klaar.</p>

                        <p>
                            020 - 760 50 70 Stuur een mail
                            Openingstijden
                        </p>

                        <p>Maandag t/m vrijdag 09.00 - 21.30 uur</p>

                        <p>Zaterdag 10.00 - 16.00 uur</p>

                        <p>Zondag 13.00 - 19.00 uur</p>
                    </div>
                
                </div>
            </div>
        </div>
    </div>
</section>
<section id="footer">
    <div class="container-fluid ">
        <div class="row">
            <div class="footer_top">
            </div>
        </div>
        <div class="row">
            <div class="footer_middle">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="widget">
                                <h4 class="title">Over</h4>

                                <div class="widget-content">
                                    <p>
                                        <a class="" href="#"> <img class="logo" src="assets/img/logo.png"
                                                                   alt="logo"/></a></p>

                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                        Phasellus sollicitudin sem nec leo tempus fermentum.
                                        Integer consequat, turpis eget lobortis.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="widget bar">
                                <h4 class="title">VOLG ONS</h4>

                                <div class="widget-content">
                                    <ul class="social">
                                        <li>
                                            <i class="fa fa-facebook"></i>
                                            <a href="#http://facebook.com">Facebook</a>
                                        </li>
                                        <li>
                                            <i class="fa fa-twitter"></i>
                                            <a href="#http://twitter.com">Twitter</a></li>
                                        <li>
                                            <i class="fa fa-google-plus"></i>
                                            <a href="#http://google.com">Google</a>
                                        </li>
                                        <li>
                                            <i class="fa fa-instagram"></i>
                                            <a href="#http://instagram.com">Instagram</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="widget">
                                <h4 class="title">Contact informatie</h4>

                                <div class="widget-content">
                                    <p> Telefoon: 0900 1234 </p>

                                    <p>Fax: 0900 1234 </p>

                                    <p>E-mail: info@ticketslam.nl </p>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="footer_bottom">
                <div class="container">
                    <ul class="footer_menu">
                        <li><a href="">Terms of Service</a></li>
                        <li><a href="">Privacy Policy </a></li>
                        <li><a href="">Creative Rights Policy</a></li>
                        <li><a href="">Contact Us</a></li>
                        <li><a href="">Support & FAQ</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script>window.jQuery || document.write('<script src="assets/js/vendor/jquery-1.11.3.min.js"><\/script>')</script>
<script src="assets/js/listgridview.js"></script>
<script src="assets/js/main.js"></script>

<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
    (function (b, o, i, l, e, r) {
        b.GoogleAnalyticsObject = l;
        b[l] || (b[l] =
                function () {
                    (b[l].q = b[l].q || []).push(arguments)
                });
        b[l].l = +new Date;
        e = o.createElement(i);
        r = o.getElementsByTagName(i)[0];
        e.src = 'https://www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e, r)
    }(window, document, 'script', 'ga'));
    ga('create', 'UA-XXXXX-X', 'auto');
    ga('send', 'pageview');
</script>
</body>
</html>
