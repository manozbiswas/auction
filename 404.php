<?php
get_header();
?>
<section id="main_content">
    <div class="container-fluid">
        <div class="row">
            <div class="main_tag_line">
                &nbsp;
            </div>
        </div>
    </div>
    <div class="container content-wrap">
        <div class="row">
            <div class="col-md-3">
                <div class="sidebar">
                    <?php get_template_part('sidebar');?>
                </div>
            </div>
            <div class="col-md-9">
                <div class="products contents">
                    <div id="listgrid " class="error-404">
                        <h1 style="margin-top: 50px;font-size: 50px;">404 error</h1>
                        <p>Sorry, something you looking for is not found</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>
