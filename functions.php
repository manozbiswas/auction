<?php

require_once dirname(__FILE__) . '/lib/require.php';

echo get_query_var('pagename');
////global $myOptions;
//print_r($myOptions); die();
/**
 * add script to wordpress by enqueue method
 */
function auctionLoadScripts()
{
    if (!is_admin()) {

        wp_enqueue_style('normalize', get_template_directory_uri() . '/assets/css/normalize.css');
        wp_enqueue_style('fontawesome', get_template_directory_uri() . '/assets/css/font-awesome.min.css');
        wp_enqueue_style('bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css');
        wp_enqueue_style('listgridview', get_template_directory_uri() . '/assets/css/listgridview.css');
        wp_enqueue_style('stylesheet', get_stylesheet_uri());
        // comment out the next two lines to load the local copy of jQuery
        wp_deregister_script('jquery');
        wp_register_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js', false, '1.11.3');
        wp_enqueue_script('jquery');

        wp_enqueue_script('modernizer', get_template_directory_uri() . '/assets/js/vendor/modernizr-2.8.3.min.js', array('jquery'), '');
        wp_enqueue_script('bootstrap_js', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array('jquery'), '');
        wp_enqueue_script('listgridview', get_template_directory_uri() . '/assets/js/listgridview.js', array('jquery'), '');
        wp_enqueue_script('main-js', get_template_directory_uri() . '/assets/js/main.js', array('jquery'), '');
    }
}

add_action('wp_enqueue_scripts', 'auctionLoadScripts');

/**
 * Register menu
 */

function auction_register_menu()
{
    if (function_exists('register_nav_menu')) {
        register_nav_menu('main', __('Main', 'Main Menu'));
    }
}

add_action('init', 'auction_register_menu');

/**
 * Sidebars
 */
add_action('widgets_init', 'auctionSidebar');
function auctionSidebar()
{
    register_sidebar(array(
        'name' => __('Main Sidebar', 'auction-sidebar'),
        'id' => 'main-sidebar',
        'description' => __('Widgets in this area will be shown on the left side bar of the product pages.', 'theme-slug'),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widgettitle title ">',
        'after_title' => '</h3>',
    ));
}

/*
 * woocommerce
 */
add_theme_support('woocommerce');


function my_add_excerpts_to_products()
{
    add_post_type_support('product', 'excerpt');
}

//add_action('init', 'my_add_excerpts_to_products');

function custom_excerpt_length($length)
{
    return 20;
}

add_filter('excerpt_length', 'custom_excerpt_length', 999);

function addTimeLeftShortCode($atts)
{
    global $product;

    $time = '';

    if (!isset ($product))
        return;
    if ('auction' != $product->product_type)
        return;

    // $timetext = __('Time left', 'wc_simple_auctions');

    if (!$product->is_started()) {
        // $timetext = __('Starting in', 'wc_simple_auctions');
        $counter_time = $product->get_seconds_to_auction();
    } else {
        $counter_time = $product->get_seconds_remaining();
    }

    $time = '<p class="time auction-time-countdown"
			data-time="' . $counter_time . '"
			data-auctionid="' . $product->id . '" data-format="' . get_option(
            'y:o:w:d:H:M:S') . '"></p>';

    if ($product->is_closed()) {
        $time = '<p class=" time has-finished">' . __('Auction finished', 'wc_simple_auctions') . '</p>';
    }
    return $time;
}

add_shortcode('theTime', 'addTimeLeftShortCode');

function my_add_short_description()
{
    // echo get_the_title();
    $charlength = 120;
    $excerpt = get_the_excerpt();
    $content = get_the_content();
    $charlength++;
    if (mb_strlen($excerpt) > $charlength || mb_strlen($content) > $charlength) {
        $subex = mb_substr($excerpt, 0, $charlength - 5);
        $exwords = explode(' ', $subex);
        $excut = -(mb_strlen($exwords[count($exwords) - 1]));
        if ($excut < 0) {
            echo '<p class="description">' . mb_substr($subex, 0, $excut) . '</p>';
        } else {
            echo '<p class="description">' . $subex . '</p>';
        }
        echo '';
    } else {
        echo '<p class="description">' . the_excerpt() . '</p>';
    }
}

//remove_action('woocommerce_shop_loop_item_title','product_short_description');
//add_action( 'woocommerce_shop_loop_item_title', 'my_add_short_description',90 );
//
//function wpgenie_show_counter_in_loop()
//{
//
//    global $product;
//
//    $time = '';
//
//    if (!isset ($product))
//        return;
//    if ('auction' != $product->product_type)
//        return;
//
//    // $timetext = __('Time left', 'wc_simple_auctions');
//
//    if (!$product->is_started()) {
//        // $timetext = __('Starting in', 'wc_simple_auctions');
//        $counter_time = $product->get_seconds_to_auction();
//    } else {
//        $counter_time = $product->get_seconds_remaining();
//    }
//
//    $time = '<p class="time auction-time-countdown"
//			data-time="' . $counter_time . '"
//			data-auctionid="' . $product->id . '" data-format="' . get_option(
//            'y:o:w:d:H:M:S') . '"></p>';
//
//    if ($product->is_closed()) {
//        $time = '<p class=" time has-finished">' . __('Auction finished', 'wc_simple_auctions') . '</p>';
//    }
//    $time = '<span class="icon-group-btn">' . $time . ' </span>' . '<div class="list-bid-price"><span class="price">€ ' . $product->get_price() . '</span>';
//    echo $time;
//}

//remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10);
//add_action('woocommerce_after_shop_loop_item_title', 'wpgenie_show_counter_in_loop', 50);

function showTimeInterval()
{
    global $product;

    $time = '';

    if (!isset ($product))
        return;
    if ('auction' != $product->product_type)
        return;

    // $timetext = __('Time left', 'wc_simple_auctions');

    if (!$product->is_started()) {
        // $timetext = __('Starting in', 'wc_simple_auctions');
        $counter_time = $product->get_seconds_to_auction();
    } else {
        $counter_time = $product->get_seconds_remaining();
    }

    $time = '<p class="time auction-time-countdown"
			data-time="' . $counter_time . '"
			data-auctionid="' . $product->id . '" data-format="' . get_option(
            'y:o:w:d:H:M:S') . '"></p>';

    if ($product->is_closed()) {
        $time = '<p class=" time has-finished">' . __('Auction finished', 'wc_simple_auctions') . '</p>';
    }

    echo '<span class="icon-group-btn">' . $time . ' </span>';
}

function showallTogether()
{
    global $product;
    my_add_short_description();
    ?>

</div>
<div class="list-right">
    <?php
    showTimeInterval();
    echo '<div class="list-bid-price"><span class="price">€ ' . $product->get_start_price() . '</span>';
    ?>
    <!--</div>-->
    <?php
}

remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10);
add_action('woocommerce_after_shop_loop_item_title', 'showallTogether', 50);








