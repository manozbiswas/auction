<?php get_header(); ?>
<section id="main_content">
    <div class="container-fluid">
        <div class="row">
            <div class="main_tag_line">
                &nbsp;
            </div>
        </div>
    </div>
    <div class="container content-wrap">
        <div class="row">
            <?php get_template_part('sidebar'); ?>
            <div class="col-md-9">
                <div class="products contents">
                    <div class="single-product">
                        <?php if (have_posts()): ?>
                        <?php while (have_posts()) :
                        the_post(); ?>
                        <div class="row">
                            <div class="col-md-7">
                                <div class="product-content">
                                    <div class="product-info">
                                        <div class="product-image">
                                            <img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>"
                                                 height="300px" width="250px" alt="Single Product"/>
                                        </div>
                                        <div class="product-description">
                                            <h3 class="product-title"> <?php the_title(); ?></h3>

                                            <p>
                                                <?php echo apply_filters('woocommerce_short_description', $post->post_excerpt); ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="product-bid">
                                    <div class="bid">
                                        <h2 class="tag-line">Bied mee en win!</h2>

                                        <p class="time"><?php echo do_shortcode('[theTime]'); ?></p>

                                        <div class="my-bid">
                                            <form method="post" action="">
                                                <div class="form-group">
                                                    <input type="number" name="bidValue" class="form-control"
                                                           min="<?php echo $product->get_curent_bid(); ?>"
                                                           max="<?php echo $product->get_reserved_price() ?>"
                                                           step="<?php echo $product->get_increase_bid_value(); ?>"
                                                           value="<?php echo $product->get_start_price(); ?>"
                                                           placeholder="€ Mijn Bod" required>
                                                </div>
                                                <button type="submit" class="button_red btn btn-default bidme">Plaats
                                                    Bod
                                                </button>

                                            </form>
                                        </div>
                                        <div class="place-bid">
                                            <div class="available-bids">
                                                <p class="low-bid">Het bod is te laag. Het minimum bod is € 15</p>

                                                <p class="bids"><span>€ 25 </span>Henk Jacksen 14:33:52</p>

                                                <p class="bids"><span>€ 25 </span>Henk Jacksen 14:33:52</p>

                                                <p class="exclusive">Biedingen zijn exclusief € 5,- administratiekosten.
                                                    Als je wint, ben je verplicht te betalen.
                                                </p>
                                            </div>
                                            <div class="buy-now btn"><a href=""> Of koop nu voor € 37 </a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="product-tab">
                                    <ul class="nav nav-tabs nav-justified" id="tab_details">
                                        <li role="presentation" class="active"><a href="#details">Details</a></li>
                                        <li role="presentation"><a href="#more_info">Meer Info</a></li>
                                        <li role="presentation"><a href="#location">Locatie</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="details tab-pane fade in active" id="details">
                                            <?php the_content(); ?>
                                        </div>
                                        <div class="more-info tab-pane fade" id="more_info">
                                            <?php $pnote =  get_post_meta( $product -> id, '_purchase_note', true );?>
                                           <?php echo $pnote!= '' ? ' <p class="purchase-note"><span>Purchase Note: </span>'. get_post_meta( $product -> id, '_purchase_note', true ) .'</p>' : '';?>
                                        </div>
                                        <div class="location tab-pane fade" id="location">
                                            <p>sollicitudin mi quis, ultrices nunc. Duis viverra risus id dui tempus
                                                laoreet.
                                                Integer lao
                                            </p>

                                            <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus eget
                                                orci
                                                vitae augue gravida fermentum.
                                                Morbi nec orci fringilla, sollicitudin mi quis, ultrices nunc. Duis
                                                viverra
                                                risus id dui tempus laoreet
                                                . Integer laoreet ac nulla in pellentesque. Pellentesque sagittis elit
                                                nisi,
                                                nec imperdiet risus ultricies efficitur
                                            </p>

                                            <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                                Phasellus eget orci vitae augue gravida fermentum. Morbi nec orci
                                                fringilla,
                                            </p>

                                            <p>reet ac nulla in pellentesque. Pellentesque sagittis elit nisi, nec
                                                imperdiet
                                                risus ultricies efficitur </p>
                                        </div>
                                    </div>
                                </div>
                                <div id="listgrid">
                                    <div class="products recent-products">
                                        <h3>MEEST VERKOCHT TICKETS </h3>
                                        <?php
                                        $terms = wp_get_post_terms($post->ID, 'product_cat');
                                        $tags = wp_get_post_terms($post->ID, 'product_tag');
//                                        var_dump($tags);
                                        foreach ($tags as $tag) {
                                            $tags_array[] = $tag->term_id;

                                        }
                                        foreach ($terms as $term) {
                                            $cats_array[] = $term->term_id;
                                            //echo $term->name;
                                        }
                                        //echo $post->ID;
                                        //echo implode(',',$cats_array);
                                        $query_args = array(
                                            'post__not_in' => array($post->ID),
                                            'posts_per_page' => 6,
                                            'no_found_rows' => 1,
                                            'post_status' => 'publish',
                                            'post_type' => 'product',
                                            'orderby' => 'rand',
                                            'tax_query' => array(
                                                'relation' => 'OR',
                                                array(
                                                    'taxonomy' => 'product_type',
                                                    'field' => 'slug',
                                                    'terms' => 'auction'
                                                ),
                                                array(
                                                    'taxonomy' => 'product_cat',
                                                    'field' => 'term_id',
                                                    'terms' => $cats_array
                                                ),
                                                array(
                                                    'taxonomy' => 'product_tag',
                                                    'field' => 'term_id',
                                                    'terms' => $tags_array
                                                )
                                            ),
                                            'auction_arhive' => TRUE,
                                            'meta_query' => array(
                                                array(
                                                    'key' => '_visibility',
                                                    'value' => array('catalog', 'visible'),
                                                    'compare' => 'IN'
                                                ),
                                            )
                                        );
                                        $r = new WP_Query($query_args);
                                        ?>
                                        <?php if ($r->have_posts()) : ?>
                                            <ul class="grid">
                                                <?php while ($r->have_posts()) : $r->the_post(); ?>
                                                    <li class="product">
                                                        <div class="list-image">
                                                            <a href="<?php echo the_permalink();?>"><img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>" height="200px"
                                                                 width="250px"/></a>
                                                        </div>
                                                        <div class="list-style">
                                                            <div class="list-left">
                                                                <span class="title"><a href="<?php echo the_permalink();?>"><?php echo the_title();?></a></span>
                                                                <div class="icon-group-btn">
                                                                    <?php echo do_shortcode('[theTime]'); ?>
                                                                </div>
                                                            </div>
                                                            <div class="list-right">
                                                                <span class="price"><?php echo '€ '.$product->get_start_price();?></span>
                                                    <span class="detail"><a href="<?php echo the_permalink();?>" class="btn btn-default button_red">Bied
                                                            mee</a></span>
                                                            </div>
                                                        </div>
                                                    </li>
                                                <?php endwhile; ?>
                                            </ul>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>
