<?php get_header(); ?>
<section id="main_content">
    <div class="container-fluid">
        <div class="row">
            <div class="main_tag_line">
                &nbsp;
            </div>
        </div>
    </div>
    <div class="container content-wrap">
        <div class="row">
            <div class="col-md-3">
                <div class="sidebar">
                    <?php get_template_part('sidebar');?>
                </div>
            </div>
            <div class="col-md-9">
                <div class="products contents">
                    <?php
                    if (have_posts()) :
                        while (have_posts()) :
                            the_title();
                            the_post();
                            the_content();
                        endwhile;
                    endif;
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>
