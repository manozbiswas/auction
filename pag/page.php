<?php get_header(); ?>
<section id="main_content">
    <div class="container-fluid">
        <div class="row">
            <div class="main_tag_line">
                &nbsp;
            </div>
        </div>
    </div>
    <div class="container content-wrap">
        <div class="row">
            <div class="col-md-3">
                <div class="sidebar">
                    <?php get_template_part('sidebar');?>
                </div>
            </div>
            <div class="col-md-9">
                <div class="view-controls-list" id="viewcontrols">
                    <a class="gridview active"><i class="fa fa-th fa-2x"></i></a>
                    <a class="listview "><i class="fa fa-list fa-2x"></i></a>
                </div>
                <div class="products contents">
                    <div id="listgrid">
                        <?php //if ( have_posts() ) :?>
                        <?php// woocommerce_content(); ?>
                        <?php echo do_shortcode('[recent_auctions per_page="12" columns="4" orderby="date" order="desc"]') ?>

                        <?php // endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>
