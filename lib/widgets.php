<?php

/**
 * Class MyNewWidget
 * Create custom widget
 */
class ProductCategoryWidget extends WP_Widget
{

    function __construct()
    {
        // Instantiate the parent object
        parent::__construct(
        // base ID of the widget
            'product-category-widget',

            // name of the widget
            __('Product Categories', 'auction'),

            // widget options
            array(
                'description' => __('This widget shows all the product categories available  ', 'auction')
            )
        );
    }

    function widget($args, $instance)
    {
        echo $args['before_widget'];
        $depth = !empty($instance['category']) ? $instance['category'] : 10;
        if ( ! empty( $instance['title'] ) ) {
            echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
        }
        $taxonomy     = 'product_cat';
        $orderby      = 'name';
        $show_count   = $depth;      // 1 for yes, 0 for no
        $pad_counts   = 0;      // 1 for yes, 0 for no
        $hierarchical = 1;      // 1 for yes, 0 for no
        $title        = '';
        $empty        = 0;

        $category_args = array(
            'taxonomy'     => $taxonomy,
            'orderby'      => $orderby,
            'show_count'   => $show_count,
            'pad_counts'   => $pad_counts,
            'hierarchical' => $hierarchical,
            'title_li'     => $title,
            'hide_empty'   => $empty,
            'number'     => $depth,
        );
        $product_categories = get_categories( $category_args );
        //var_dump($product_categories);
        $count = count($product_categories);
        ?>
        <?php if($count > 0):?>
        <ul class="nav nav-pills nav-stacked widget_menu">
            <?php foreach( $product_categories as $product_category):?>
<!--                --><?php //var_dump($product_category);?>
                <li>
                    <a href="<?php echo get_term_link( $product_category);?>">
                        <?php echo $product_category->name ; ?>
                    </a>
                </li>
            <?php endforeach;?>
        </ul>
        <?php endif;?>
  <?php
        echo $args['after_widget'];
    }

    function update($new_instance, $old_instance)
    {

        $instance = $old_instance;
        $instance['category'] = strip_tags($new_instance['category']);
        $instance['title'] = strip_tags($new_instance['title']);
        return $instance;
    }

    function form($instance)
    {
        $depth = !empty($instance['category']) ? $instance['category'] : __('10', 'text_domain');
        $title = !empty($instance['title']) ? $instance['title'] : __('New title', 'text_domain');

        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>"
                   name="<?php echo $this->get_field_name('title'); ?>" type="text"
                   value="<?php echo esc_attr($title); ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('category'); ?>"><?php _e('Number of category to display:'); ?></label>
            <input class="widefat" type="text" id="<?php echo $this->get_field_id('category'); ?>"
                   name="<?php echo $this->get_field_name('category'); ?>" value="<?php echo esc_attr($depth); ?>">
        </p>

    <?php

    }
}

function auctionRegisterWidgets()
{
    register_widget('ProductCategoryWidget');
    register_widget('DynamicCustomSearchWidget');
}

add_action('widgets_init', 'auctionRegisterWidgets');

class DynamicCustomSearchWidget extends WP_Widget
{
    function __construct()
    {
        // Instantiate the parent object
        parent::__construct(
        // base ID of the widget
            'dynamic-search-widget',

            // name of the widget
            __('Custom Search', 'auction'),

            // widget options
            array(
                'description' => __('This widget shows the custom search option in the sidebar ', 'auction')
            )
        );
    }

    function widget($args, $instance)
    {
        ?>
        <div class="search_bar">
            <form method="post" action="">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="zoek veld "/>
                    <span class="input-group-btn">
                        <button name="submit" class="btn btn-default button_red" type="submit">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </span>
                </div>
                <!-- /input-group -->
            </form>
        </div>
    <?php
    }

    function update($new_instance, $old_instance)
    {

        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        return $instance;
    }

    function form($instance)
    {
        $title = !empty($instance['title']) ? $instance['title'] : __('New title', 'text_domain');
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>"
                   name="<?php echo $this->get_field_name('title'); ?>" type="text"
                   value="<?php echo esc_attr($title); ?>">
        </p>

    <?php

    }
}

