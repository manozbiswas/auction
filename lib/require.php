<?php
/**
 * Created by PhpStorm.
 * User: MANOZ
 * Date: 9/7/2015
 * Time: 12:14 PM
 */
/*
 * Redux frameork
 */

if ( !class_exists( 'ReduxFramework' ) && file_exists( dirname( __FILE__ ) . '/redux-framework/ReduxCore/framework.php' ) ) {
    require_once( dirname( __FILE__ ) . '/redux-framework/ReduxCore/framework.php' );

}
if ( !isset( $redux_demo ) && file_exists( dirname( __FILE__ ) . '/redux-framework/redux-theme-config.php' ) ) {
    require_once(dirname(__FILE__) . '/redux-framework/redux-theme-config.php');
}

require_once dirname( __FILE__ ) . '/widgets.php';
////require_once dirname( __FILE__ ) . '/custom-post-type.php';
////require_once dirname( __FILE__ ) . '/custom-taxonomy.php';
//require_once dirname( __FILE__ ) . '/cmb/meta-boxes.php';
//require_once dirname( __FILE__ ) . '/cmb/my-meta-box.php';
//require_once dirname( __FILE__ ) . '/wp_bootstrap_navwalker.php';
